# emux-tunnel

emux-tunnel is [emux](https://bitbucket.org/blacktear23/emux) based TCP tunnel programe. You can use emux-tunnel create a encrypted tunnel on two private network and let server on one private network to access service that in other private network.

```
+-------------------+                          +-------------------+
| Private Network A |                          | Private Network B |
|   +----------+    | +--------+    +--------+ |    +----------+   |
|   | Server A |----+-| Tunnel |<-->| Tunnel |-+----| Server B |   |
|   +----------+    | | Server |    | Client | |    +----------+   |
+-------------------+ +--------+    +--------+ +-------------------+
```

## Command Line Parameters

```
Usage of ./emux-tunnel:
  -P	Enable profile, it will create emux-tunnel.prof file.
  -c string
    	Config file json
  -m string
    	Mode (s|c|r), s: server, c: client, r: redirect (default "s")
  -v	Print version
  -w string
    	White list file
  -y string
    	Config file yaml
```

## Config File

emux-tunnel configuration file is a JSON format file. Below is an example:

```
{
    "ServerAddr": "111.111.111.111:2020",
    "Cipher": "aes-128-gcm",
    "Password": "password",
    "PoolSize": 10,
    "RedirServerAddr": "0.0.0.0:7070",
    "Services": [
        {
            "LocalAddr": "127.0.0.1:8080",
            "RemoteAddr": "192.168.1.100:80"
        }
    ]
}
```

YAML format:

```
---
server_addr: "111.111.111.111:2020"
chipher: aes-128-gcm
password: password,
pool_size: 10
redir_server_addr: 0.0.0.0:7070
services:
  - local_addr: 127.0.0.1:8080
    remote_addr: 192.168.1.100:80
```

### For Server Mode

In server mode, only ServerAddr, Cipher, Password is required.

- ServerAddr: Server listen address.
- Cipher: Cipher type for emux. (aes-128-gcm, aes-192-gcm, aes-256-gcm)
- Password: Encryption password.

### For Client Mode

In client mode, ServerAddr, Cipher, Password, PoolSize and Services is all required.

- ServerAddr: emux-tunnel server address.
- Cipher: Cipher type for emux. (aes-128-gcm, aes-192-gcm, aes-256-gcm)
- Password: Encryption password.
- PoolSize: emux connection pool size, if not exist default is 100. PoolSize is for each Service
- Services: Services is an array for local address and target server address pair(we call this pair as Service).
  - LocalAddr: local listen address
  - RemoteAddr: target server address

### For Redirect Mode

In redirect mode, ServerAddr, Cipher, Password, PoolSize and RedirServerAddr is required.

- ServerAddr: emux-tunnel server address.
- Cipher: Cipher type for emux. (aes-128-gcm, aes-192-gcm, aes-256-gcm)
- Password: Encryption password.
- PoolSize: emux connection pool size, if not exist default is 100. PoolSize is for each Service
- RedirServerAddr: redirect server listen address, recommand is "0.0.0.0:7070"

## Redirect Mode

emux-tunnel support redirect mode only for Linux platform and this can be work with iptables redirect module.

```
iptables -t nat -N emux
iptables -t nat -A emux -p tcp --dport 2020 -j RETURN
iptables -t nat -A emux -p tcp -j REDIRECT --to-ports 7070
iptables -t nat -A OUTPUT -p tcp -d 192.168.1.0/24 -j emux
iptables -t nat -A PREROUTING -p tcp -d 192.168.1.0/24 -j emux
```

You can use ipset to make redirect address more flexible

```
ipset create emux hash:net maxelem 65536
ipset add emux 192.168.1.0/24

iptables -t nat -N EMUX
iptables -t nat -A EMUX -p tcp --dport 2020 -j RETURN
iptables -t nat -A EMUX -p tcp -j REDIRECT --to-ports 7070
iptables -t nat -A OUTPUT -p tcp -m set --match-set emux dst -j EMUX
iptables -t nat -A PREROUTING -p tcp -m set --match-set emux dst -j EMUX
```

## White List File

White list file is only effect by server mode. It limit client can accessed IP-Ports.

```
192.168.1.100:80
192.168.1.100:22
```

You can `kill -HUP` to reload white list file on the fly. If you not specify white list file, white list filter will be disable.
