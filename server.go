package main

import (
	"log"
	"net"

	"bitbucket.org/blacktear23/emux"
)

type TunnelServer struct {
	cfg     *Config
	emuxCfg *emux.Config
}

func NewTunnelServer(cfg *Config) *TunnelServer {
	return &TunnelServer{
		cfg:     cfg,
		emuxCfg: cfg.GetEmuxConfig(),
	}
}

func (s *TunnelServer) processStream(conn net.Conn) {
	host, err := DecodeTunnelHeader(conn)
	if err != nil {
		log.Println("Error on parse header:", err)
		return
	}
	if FilterHost(host) {
		log.Println("Host", host, "is not in white list")
		conn.Close()
		return
	}
	remote, err := DialTarget(host)
	if err != nil {
		log.Printf("Dial to %s error: %v", host, err)
		conn.Close()
		return
	}
	ProxyPipe(conn, remote)
}

func (s *TunnelServer) processConn(conn net.Conn) {
	session, err := emux.Server(conn, s.emuxCfg)
	if err != nil {
		log.Println(err)
		return
	}
	for {
		stream, err := session.Accept()
		if err != nil {
			log.Println(err)
			break
		}
		go s.processStream(stream)
	}
}

func (s *TunnelServer) Run() {
	listener, err := net.Listen("tcp", s.cfg.ServerAddr)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Listen on:", s.cfg.ServerAddr)
	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Println(err)
			break
		}
		go s.processConn(conn)
	}
}
