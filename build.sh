#!/bin/sh
BASE_DIR=`pwd`/`dirname $0`
BUILD_DIR=${BASE_DIR}/build
LDFLAGS="-s -w"

build_target() {
    name=$1
    os=$2
    arch=$3
    build_path=${BUILD_DIR}/${os}-${arch}
    suffix=""
    if [ "${os}" = "windows" ]; then
        suffix=".exe"
    fi
    output_path=${build_path}/${name}${suffix}
    module_path=${BASE_DIR}
    echo Build $name for ${os}-${arch} to $output_path
    cd $module_path; GOOS=$os GOARCH=$arch go build -ldflags "${LDFLAGS}" -o $output_path
    cp ${BASE_DIR}/scripts/* ${build_path}
    mv ${build_path}/config.yaml.example ${build_path}/config.yaml
}

build_releases() {
    build_target emux-tunnel darwin amd64
    build_target emux-tunnel linux amd64
    build_target emux-tunnel windows amd64
}

clean_build() {
    rm -rf $BUILD_DIR
}

case $1 in
    build)
        build_releases
        ;;
    clean)
        clean_build
        ;;
    *)
        echo "build.sh (build|clean)"
        ;;
esac
