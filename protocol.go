package main

import (
	"encoding/binary"
	"errors"
	"io"
	"net"
	"strconv"
)

const (
	TypeIPv4     = 1
	TypeIPv6     = 2
	LenType      = 1
	LenIPv4      = net.IPv4len + 2
	LenIPv6      = net.IPv6len + 2
	MaxHeaderLen = LenType + LenIPv6
)

/*
 * Header Format
 * +---------+-------------------+---------+
 * | TYPE 1B | IP Addr (Dynamic) | Port 2B |
 * +---------+-------------------+---------+
 * Type: 0x01 IPv4
 *       0x02 IPv6
 */
func DecodeTunnelHeader(conn net.Conn) (host string, err error) {
	buf := make([]byte, MaxHeaderLen)
	if _, err = io.ReadFull(conn, buf[:LenType]); err != nil {
		return
	}
	addrType := buf[0]
	var addrLen int = 0
	switch addrType {
	case TypeIPv4:
		addrLen = LenIPv4
	case TypeIPv6:
		addrLen = LenIPv6
	default:
		addrLen = 0
	}
	if addrLen == 0 {
		err = errors.New("Unknown Type")
		return
	}
	reqStart := 1
	reqEnd := reqStart + addrLen
	if _, err = io.ReadFull(conn, buf[reqStart:reqEnd]); err != nil {
		return
	}
	switch addrType {
	case TypeIPv4:
		host = net.IP(buf[reqStart : reqStart+net.IPv4len]).String()
	case TypeIPv6:
		host = net.IP(buf[reqStart : reqStart+net.IPv6len]).String()
	}
	port := binary.BigEndian.Uint16(buf[reqEnd-2 : reqEnd])
	host = net.JoinHostPort(host, strconv.Itoa(int(port)))
	return
}

func EncodeTunnelHeader(addr string) []byte {
	tcpAddr, _ := net.ResolveTCPAddr("tcp", addr)
	addrLen := len(tcpAddr.IP)
	buf := make([]byte, addrLen+3)
	if addrLen == net.IPv4len {
		buf[0] = TypeIPv4
	} else if addrLen == net.IPv6len {
		buf[0] = TypeIPv6
	}
	copy(buf[1:], tcpAddr.IP)
	copy(buf[1+addrLen:], Port2Byte(tcpAddr.Port))
	return buf
}

func Port2Byte(port int) []byte {
	ret := make([]byte, 2)
	binary.BigEndian.PutUint16(ret, uint16(port))
	return ret
}
