package main

import (
	"flag"
	"io"
	"log"
	"net"
	"os"
	"os/signal"
	"runtime/pprof"
	"syscall"
	"time"
)

type ReloadHandler func()

func WaitSignal(profile bool, onReload ReloadHandler) {
	var sigChan = make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt, os.Kill, syscall.SIGHUP)
	for sig := range sigChan {
		if sig == syscall.SIGHUP {
			if onReload != nil {
				onReload()
			}
		} else {
			if profile {
				pprof.StopCPUProfile()
			}
			log.Fatal("Server Exit\n")
		}
	}
}

func EnableProfile(fileName string) bool {
	ret := true
	fp, err := os.Create(fileName)
	if err != nil {
		log.Println(err)
		ret = false
	} else {
		pprof.StartCPUProfile(fp)
	}
	return ret
}

func CheckFile(fileName string) {
	fstat, err := os.Stat(fileName)
	if err != nil {
		log.Println(err)
		flag.PrintDefaults()
		os.Exit(1)
	}
	if fstat.IsDir() {
		log.Printf("File %s is directory", fileName)
		flag.PrintDefaults()
		os.Exit(1)
	}
}

func ProxyPipe(p1, p2 io.ReadWriteCloser) {
	defer p1.Close()
	defer p2.Close()

	// start tunnel
	p1die := make(chan struct{})
	go func() {
		io.Copy(p1, p2)
		close(p1die)
	}()

	p2die := make(chan struct{})
	go func() {
		io.Copy(p2, p1)
		close(p2die)
	}()

	// wait for tunnel termination
	select {
	case <-p1die:
	case <-p2die:
	}
}

func DialTarget(host string) (net.Conn, error) {
	// TODO: Implements get laddr for mass connection support
	var laddr net.Addr = nil
	dialer := net.Dialer{
		Timeout:   10 * time.Second,
		LocalAddr: laddr,
	}
	return dialer.Dial("tcp", host)
}
