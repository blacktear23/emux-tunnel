package main

import (
	"flag"
	"log"
	"os"
)

const VERSION = "1.0"

func main() {
	log.SetOutput(os.Stdout)
	var config, configYaml, mode, whiteList string
	var profile, ver bool
	flag.BoolVar(&profile, "P", false, "Enable profile, it will create emux-tunnel.prof file.")
	flag.BoolVar(&ver, "v", false, "Print version")
	flag.StringVar(&mode, "m", "s", "Mode (s|c|r), s: server, c: client, r: redirect")
	flag.StringVar(&config, "c", "", "Config file json")
	flag.StringVar(&configYaml, "y", "", "Config file yaml")
	flag.StringVar(&whiteList, "w", "", "White list file")
	flag.Parse()

	if ver {
		log.Println("emux-tunnel Version:", VERSION)
		os.Exit(0)
	}

	if profile {
		EnableProfile("emux-tunnel.prof")
	}

	var (
		cfg *Config
		err error
	)

	if configYaml != "" {
		CheckFile(configYaml)
		cfg, err = NewConfig(configYaml, FTYPE_YAML)
	} else {
		CheckFile(config)
		cfg, err = NewConfig(config, FTYPE_JSON)
	}

	if err != nil {
		flag.PrintDefaults()
		log.Fatal(err)
	}

	if mode == "s" {
		log.Println("Start Server Mode")
		if whiteList != "" {
			if err := LoadWhiteListFromFile(whiteList); err != nil {
				log.Println(err)
			} else {
				log.Println("Load white list from:", whiteList)
			}
		}
		s := NewTunnelServer(cfg)
		go s.Run()
	} else if mode == "c" {
		log.Println("Start Client Mode")
		for _, service := range cfg.Services {
			c := NewTunnelClient(service, cfg)
			go c.Run()
		}
	} else if mode == "r" {
		log.Println("Start Redirect Mode")
		c := NewRedirClient(cfg)
		go c.Run()
	} else {
		log.Fatal("Unknown Mode")
	}
	WaitSignal(profile, func() {
		if mode == "s" && whiteList != "" {
			if err := LoadWhiteListFromFile(whiteList); err != nil {
				log.Println(err)
			} else {
				log.Println("Reload white list from:", whiteList)
			}
		}
	})
}
