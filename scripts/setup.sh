#!/bin/bash

install_binarys() {
    mkdir -p /usr/local/bin
    mkdir -p /etc/emux-tunnel
    cp ./config.yaml /etc/emux-tunnel/config.yaml
    cp ./emux-tunnel /usr/local/bin
}

install_client() {
    cp ./emux-tunnel-client.service /etc/systemd/system/
    systemctl daemon-reload
    systemctl enable emux-tunnel-client
    echo "Start service using: systemctl start emux-tunnel-client"
}

install_redir() {
    cp ./emux-tunnel-redir.service /etc/systemd/system/
    systemctl daemon-reload
    systemctl enable emux-tunnel-redir
    echo "Start service using: systemctl start emux-tunnel-redir"
}

install_server() {
    touch /etc/emux-tunnel/whitelist.txt
    cp ./emux-tunnel-server.service /etc/systemd/system/
    systemctl daemon-reload
    systemctl enable emux-tunnel-server
    echo "Start service using: systemctl start emux-tunnel-server"
}

case $1 in
    client)
        install_binarys
        install_client
        ;;
    server)
        install_binarys
        install_server
        ;;
    redir)
        install_binarys
        install_redir
        ;;
    *)
        echo "./setup.sh (server | client | redir)"
        ;;
esac