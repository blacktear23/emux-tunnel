package main

import (
	"encoding/json"
	"errors"
	"os"

	"bitbucket.org/blacktear23/emux"
	"gopkg.in/yaml.v2"
)

const (
	FTYPE_JSON = 1
	FTYPE_YAML = 2
)

type Service struct {
	LocalAddr  string `json:"LocalAddr" yaml:"local_addr"`
	RemoteAddr string `json:"RemoteAddr" yaml:"remote_addr"`
}

type Config struct {
	ServerAddr      string    `json:"ServerAddr" yaml:"server_addr"`
	Cipher          string    `json:"Cipher" yaml:"cipher"`
	Password        string    `json:"Password" yaml:"password"`
	PoolSize        int       `json:"PoolSize" yaml:"pool_size"`
	Services        []Service `json:"Services" yaml:"services"`
	RedirServerAddr string    `json:"RedirServerAddr" yaml:"redir_server_addr"`
	configFile      string
	emuxCfg         *emux.Config
	ftype           int
}

func NewConfig(cfgFile string, ftype int) (*Config, error) {
	ret := &Config{
		configFile: cfgFile,
		emuxCfg:    nil,
		ftype:      ftype,
	}
	err := ret.Reload()
	return ret, err
}

func (c *Config) Reload() error {
	fp, err := os.Open(c.configFile)
	if err != nil {
		return err
	}
	defer fp.Close()
	switch c.ftype {
	case FTYPE_JSON:
		return json.NewDecoder(fp).Decode(c)
	case FTYPE_YAML:
		return yaml.NewDecoder(fp).Decode(c)
	default:
		return errors.New("Unknown file type")
	}
}

func (c *Config) GetEmuxConfig() *emux.Config {
	if c.emuxCfg == nil {
		c.emuxCfg = emux.DefaultConfig()
		c.emuxCfg.CipherType = c.Cipher
		c.emuxCfg.Password = c.Password
	}
	return c.emuxCfg
}
