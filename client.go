package main

import (
	"log"
	"net"
	"sync"

	"bitbucket.org/blacktear23/emux"
)

var DefaultSize = 100

type TunnelClient struct {
	local   string
	remote  string
	cfg     *Config
	emuxCfg *emux.Config
	pool    *emux.Pool
	lock    sync.Mutex
	size    int
}

func NewTunnelClient(service Service, cfg *Config) *TunnelClient {
	return &TunnelClient{
		local:   service.LocalAddr,
		remote:  service.RemoteAddr,
		cfg:     cfg,
		pool:    nil,
		emuxCfg: cfg.GetEmuxConfig(),
	}
}

func (s *TunnelClient) processConn(conn net.Conn) {
	remote, err := s.dialViaServer()
	if err != nil {
		log.Println("Connect to server fail:", err)
		return
	}
	ProxyPipe(conn, remote)
}

func (s *TunnelClient) dialViaServer() (net.Conn, error) {
	if s.pool == nil {
		size := s.cfg.PoolSize
		if size == 0 {
			size = DefaultSize
		}
		pool, err := emux.NewPool(s.cfg.ServerAddr, s.emuxCfg, size)
		if err != nil {
			return nil, err
		}
		s.lock.Lock()
		if s.pool == nil {
			s.pool = pool
		} else {
			pool.Close()
		}
		s.lock.Unlock()
	}
	conn, err := s.pool.Open()
	if err != nil {
		return nil, err
	}
	header := EncodeTunnelHeader(s.remote)
	conn.Write(header)
	return conn, nil
}

func (s *TunnelClient) Run() {
	listener, err := net.Listen("tcp", s.local)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Listen on:", s.local, "Remote server:", s.remote)
	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Println(err)
			break
		}
		go s.processConn(conn)
	}
}
