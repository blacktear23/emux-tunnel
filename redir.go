package main

import (
	"errors"
	"log"
	"net"
	"sync"

	"bitbucket.org/blacktear23/emux"
)

var (
	errConnType = errors.New("Only support TCP connection")
)

type RedirClient struct {
	local   string
	cfg     *Config
	emuxCfg *emux.Config
	pool    *emux.Pool
	lock    sync.Mutex
	size    int
}

func NewRedirClient(cfg *Config) *RedirClient {
	return &RedirClient{
		local:   cfg.RedirServerAddr,
		cfg:     cfg,
		pool:    nil,
		emuxCfg: cfg.GetEmuxConfig(),
	}
}

func (c *RedirClient) processConn(conn net.Conn) {
	remoteAddr, err := getDestAddr(conn)
	if err != nil {
		log.Println("Cannot get destination address:", err)
	}
	remote, err := c.dialViaServer(remoteAddr)
	if err != nil {
		log.Println("Connect to server fail:", err)
		return
	}
	ProxyPipe(conn, remote)
}

func (c *RedirClient) dialViaServer(remoteAddr string) (net.Conn, error) {
	if c.pool == nil {
		size := c.cfg.PoolSize
		if size == 0 {
			size = DefaultSize
		}
		pool, err := emux.NewPool(c.cfg.ServerAddr, c.emuxCfg, size)
		if err != nil {
			return nil, err
		}
		c.lock.Lock()
		if c.pool == nil {
			c.pool = pool
		} else {
			pool.Close()
		}
		c.lock.Unlock()
	}
	conn, err := c.pool.Open()
	if err != nil {
		return nil, err
	}
	header := EncodeTunnelHeader(remoteAddr)
	conn.Write(header)
	return conn, nil
}

func (c *RedirClient) Run() {
	if !supportRedir() {
		log.Fatal("Redirect Mode is not supported")
	}
	listener, err := net.Listen("tcp", c.local)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Redir Server Listen on:", c.local)
	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Println(err)
			break
		}
		go c.processConn(conn)
	}
}
