package main

import (
	"errors"
	"net"
)

var (
	errRedirNotSupport = errors.New("Redirect Mode is not support")
)

func supportRedir() bool {
	return false
}

func getDestAddr(conn net.Conn) (string, error) {
	return "", errRedirNotSupport
}
