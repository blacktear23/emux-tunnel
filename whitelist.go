package main

import (
	"bufio"
	"errors"
	"os"
)

var EnableWhiteList bool
var WhiteList map[string]bool

func init() {
	EnableWhiteList = false
	WhiteList = make(map[string]bool)
}

func FilterHost(host string) bool {
	if !EnableWhiteList {
		return false
	}
	_, have := WhiteList[host]
	return !have
}

func LoadWhiteListFromFile(fpath string) error {
	fstat, err := os.Stat(fpath)
	if err != nil {
		return err
	}
	if fstat.IsDir() {
		return errors.New("White list file is directory not file")
	}

	file, err := os.Open(fpath)
	if err != nil {
		return err
	}
	defer file.Close()
	tempWhiteList := make(map[string]bool)
	reader := bufio.NewReader(file)
	for {
		line, _, _ := reader.ReadLine()
		if len(line) == 0 {
			break
		}
		tempWhiteList[string(line)] = true
	}
	if len(tempWhiteList) > 0 {
		WhiteList = tempWhiteList
		EnableWhiteList = true
	} else {
		EnableWhiteList = false
	}
	return nil
}
