module bitbucket.org/blacktear23/emux-tunnel

go 1.20

require (
	bitbucket.org/blacktear23/emux v1.0.2
	gopkg.in/yaml.v2 v2.4.0
)

require golang.org/x/crypto v0.9.0 // indirect
